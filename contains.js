module.exports = contains;

function contains(array, value) {
    var i = array.length;
    while (i--) {
        if (array[i] == value) {
            return true;
        }
    }
    return false;
}
