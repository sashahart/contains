var assert = require("assert");

var contains = require("../contains");

describe('contains', function(){
    it('returns false for empty arrays', function() {
        assert.equal(contains([], 'thing'), false);
    });
    it('returns true for just the item', function() {
        assert.equal(contains(['thing'], 'thing'), true);
    });
    it('returns true in several items', function() {
        assert.equal(contains(['foo', 'thing', 'bar'], 'thing'), true);
    });
});
